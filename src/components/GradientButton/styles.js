import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  gradientButton: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 42,
    elevation: 2
  },
  gradientButtonText: {
    color: 'white',
    fontFamily: 'Poppins-Light',
    fontSize: 18,
    height: 28
  }
});

export default styles;
