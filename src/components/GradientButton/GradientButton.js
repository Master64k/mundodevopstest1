import React, { Component } from 'react';
import {
  Text,
  TouchableOpacity
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import styles from './styles';

type Props = {
  disabled?:boolean,
  buttonText?: string,
  gradientDirection?:string

};

export default class GradientButton extends Component<Props> {
  constructor(props) {
    super(props);
  }

  static defaultProps = {
    disabled: false,
    buttonText: null,
    gradientDirection: 'vertical'
  };

  render() {
    const {
            disabled, buttonText,
            children, gradientDirection
          } = this.props;

    return (
      <TouchableOpacity
        disabled={disabled}
        onPress={this.props.onPress}>
        <LinearGradient
          style={styles.gradientButton}
          {...this.props.gradientButtonStyle}
          {...(gradientDirection === 'horizontal') ? { start: { x: 0.0, y: 0.0 }, end: { x: 0.0, y: 0.0 } } : { start: { x: 0.2, y: 0.2 }, end: { x: 0.8, y: 0.8 } }}
          colors={this.props.gradientColors}
        >
          {children}
          {
            this.props.buttonText ? <Text
              style={styles.gradientButtonText}>
              {buttonText} </Text> : null
          }

        </LinearGradient>

      </TouchableOpacity>

    );
  }
}
