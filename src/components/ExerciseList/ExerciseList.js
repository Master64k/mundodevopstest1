import React, { Component } from 'react';
import {
  View,
  FlatList,
  TouchableWithoutFeedback,
  Image
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from './styles';
import Colors from '../../assets/palettes/default-palette';
import GradientButton from '../GradientButton';
import getImage from '../../assets/images';

type Props = {
  horizontalListStyle: Object,
  horizontalData: Array,
  verticalListStyle: Object,
  verticalListRender: Function,
  style: Object,
  propertyName: string,
  gradientButtonStyle: Object,
  statusIconContainerStyle: Object,
  statusIconColor: string,
  statusIconName: string
}

export default class ExerciseList extends Component<Props> {
  constructor(props) {
    super(props);

    this.state = {
      horizontalToggle: [],
      verticalData: []
    };
  }

  componentWillMount(): void {
    const { horizontalData } = this.props;

    const buttons = [];

    horizontalData.forEach(() => {
      buttons.push({enabled: false});
    });

    this.setState({horizontalToggle: buttons});
  }

  _toggleVerticalListItems = (itens: Array, index: Number) => {
    const { verticalData, horizontalToggle } = this.state;

    if (itens.length === 0) return;

    let toggle = horizontalToggle;

    toggle[index] = {enabled: !toggle[index].enabled};
    let state = {};

    if (verticalData.filter(x => itens.includes(x)).length === 0)
    {
      state = { horizontalToggle: toggle,
        verticalData: verticalData.concat(itens) };
    } else {
      state = { horizontalToggle: toggle,
        verticalData: verticalData.filter(x => !itens.includes(x)) };
    }

    this.setState(state);
  };

  _renderHItem = ({ item, index }) => {

    const {
            propertyName,
            gradientButtonStyle,
            statusIconContainerStyle,
            statusIconColor,
            statusIconName
          } = this.props;

    const {
            horizontalToggle
          } = this.state;

    if (horizontalToggle.length === 0) return null;

    return <TouchableWithoutFeedback
            onPress={() => this._toggleVerticalListItems(item[propertyName], index)}
           >
            <View style={{ width: 70, height: 70, marginRight: 20}}>
              <GradientButton
                gradientButtonStyle={gradientButtonStyle}
                gradientColors={Colors.horizontalListGradient}

              >
                {horizontalToggle[index].enabled === true ?
                  <View style={statusIconContainerStyle}>
                    <Icon
                      name={statusIconName}
                      size={18}
                      color={statusIconColor}
                      style={{ backgroundColor: 'transparent' }}
                    />
                  </View> : null
                }
                <Image
                  source={getImage(item.image)}
                />
              </GradientButton>
            </View>
           </TouchableWithoutFeedback>;
  };

  _renderVItem = ({ item }) => {
    const { verticalListRender } = this.props;

    if (item) return verticalListRender(item);

    return null;
  };

  render() {
    const {
            horizontalListStyle,
            verticalListStyle, style,
            horizontalData
          } = this.props;

    const { verticalData } = this.state;

    return (
      <View style={[styles.container, style]}>
        <FlatList
          horizontal
          contentContainerStyle={styles.horizontalListContainer}
          style={horizontalListStyle}
          data={horizontalData}
          renderItem={this._renderHItem}
          keyExtractor={(item, index) => index.toString()}
          extraData={this.state}
        />
        <FlatList
          style={verticalListStyle}
          data={verticalData}
          keyExtractor={(item, index) => index.toString()}
          renderItem={this._renderVItem}
        />
      </View>
    );
  }
}
