import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  horizontalListContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 10
  }
});

export default styles;
