import { StyleSheet } from 'react-native';
import Colors from '../../assets/palettes/default-palette';

const styles = StyleSheet.create({

  container: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20,
    backgroundColor: Colors.background
  },
  list: {
    width: '100%',
    paddingTop: 10,
    backgroundColor: Colors.background,
    borderTopWidth: 1,
    borderTopColor: Colors.card
  },
  cardImage: {
    position: 'relative',
    top: -10,
    left: -10,
    width: 100,
    height: 100
  },
  cardTitle: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 18,
    color: Colors.text,
    textTransform: 'uppercase'
  },
  card: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: Colors.card,
    marginBottom: 10,
    padding: 10,
    borderRadius: 10
  },
  cardImageContainer: {
    borderRadius: 45,
    backgroundColor: Colors.background,
    height: 80,
    width: 80,
    marginRight: 20
  },
  cardContent: {
    flex: 1,
    justifyContent: 'space-between'
  },
  infoContainerCenter: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderLeftColor: Colors.InfoContainerBorder,
    borderRightColor: Colors.InfoContainerBorder
  },
  infoContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  icons: {
    width: 15,
    height: 15,
    marginRight: 5
  },
  infoText: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 8,
    color: Colors.text
  },
  filter: {
    minWidth: 70,
    maxWidth: 70,
    minHeight: 70,
    borderRadius: 45,
    marginHorizontal: 5
  },
  horizontalList: {
    maxHeight: 100,
    minHeight: 100,
    borderRadius: 10,
    backgroundColor: Colors.horizontalListBackground
  },
  verticalList: {
    width: '100%',
    marginTop: 20,
    paddingTop: 10
  },
  statusIconContainer: {
    backgroundColor: 'white',
    height: 20,
    width: 20,
    position: 'absolute',
    left: 55,
    top: 6,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 45
  },
  dateInfoContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'

  },
  dateInfoTextActive: {
    color: 'white',
    fontSize: 10,
    fontFamily: 'Montserrat-Regular'
  },
  dateInfoText: {
    fontSize: 10,
    color: Colors.dateInfoText,
    fontFamily: 'Montserrat-Regular'
  },
  dateInfoButton: {
    borderWidth: 1,
    borderColor: Colors.dateInfoBorder,
    backgroundColor: Colors.dateInfoBackground,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    maxHeight: 18,
    minWidth: 60,
    marginRight: 10
  },
  dateInfoButtonRed: {
    borderWidth: 1,
    borderColor: Colors.todayButton,
    backgroundColor: Colors.todayButton,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    maxHeight: 18,
    minWidth: 60,
    marginRight: 10
  },
  dateInfoButtonGreen: {
    borderWidth: 1,
    borderColor: Colors.yesterdayButton,
    backgroundColor: Colors.yesterdayButton,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    maxHeight: 18,
    minWidth: 60,
    marginRight: 10
  }

});

export default styles;
