import React, { Component } from 'react';
import {
  View,
  Text,
  Image
} from 'react-native';
import styles from './styles';
import ExerciseList from '../../components/ExerciseList';
import data from '../../../data.json';
import Colors from '../../assets/palettes/default-palette';
import getImage from '../../assets/images';
import GradientButton from '../../components/GradientButton';

export default class Profile extends Component {
  static navigationOptions = () => {
    return {
      title: 'Meu Perfil'
    };
  };

  _renderHItem = item => {

    return (
      <View style={{ width: 60, height: 60, marginRight: 20}}>
        <GradientButton
          gradientButtonStyle={styles.filter}
          gradientColors={Colors.horizontalListGradient}
          gradientDirection={'horizontal'}
        >
          <Image
            source={getImage(item.image)}
          />
        </GradientButton>
      </View>
    );
  };

  _renderVItem = item => {
    const time = (item.time % 60) === 0 ? `${(item.time / 60).toString()} h` : `${item.time} m`;

    return (
      <View>
        <View style={styles.card}>
          <View style={styles.cardImageContainer}>
            <Image
              style={styles.cardImage}
              source={getImage(item.image)}
            />
          </View>
          <View style={styles.cardContent}>
            <Text style={styles.cardTitle}>{item.name}</Text>
            <View style={{flexDirection: 'row'}}>
              <View style={styles.infoContainer}>
                <Image
                  style={styles.icons}
                  source={require('../../assets/images/ic_bike.png')}
                />
                <Text style={styles.infoText}>
                  {`${item.calories} Kcal`}
                </Text>
              </View>
              <View style={styles.infoContainerCenter}>
                <Image
                  style={styles.icons}
                  source={require('../../assets/images/ic_time.png')}
                />
                <Text style={styles.infoText}>
                  {time}
                </Text>
              </View>
              <View style={styles.infoContainer}>
                <Image
                  style={styles.icons}
                  source={require('../../assets/images/ic_balance.png')}
                />
                <Text style={styles.infoText}>
                  {`${item.weight} Kg` }
                </Text>
              </View>
            </View>
            <View style={styles.dateInfoContainer}>
              <View
                style={item.when === 'today' ? styles.dateInfoButtonRed : styles.dateInfoButton}
              >
                <Text style={ item.when === 'today' ? styles.dateInfoTextActive : styles.dateInfoText}>Hoje</Text>
              </View>
              <View
                style={item.when === 'yesterday' ? styles.dateInfoButtonGreen : styles.dateInfoButton}
              >
                <Text style={ item.when === 'yesterday' ? styles.dateInfoTextActive : styles.dateInfoText }>Ontem</Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <ExerciseList
          style={styles.list}
          horizontalListStyle={styles.horizontalList}
          horizontalData={data.filters}
          horizontalListRender={this._renderHItem}
          verticalListStyle={styles.verticalList}
          verticalListRender={this._renderVItem}
          gradientButtonStyle={styles.filter}
          propertyName={'exercises'}
          statusIconContainerStyle={styles.statusIconContainer}
          statusIconColor={Colors.yesterdayButton}
          statusIconName={'check-circle'}
        />
      </View>
    );
  }
}
