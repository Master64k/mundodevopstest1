import {
  createAppContainer,
  createStackNavigator
} from 'react-navigation';
import Profile from '../screens/Profile';
import StackNavigationConfig from './config/stack/StackNavigationConfig';

const RootNavigation = createStackNavigator(
  {
   Profile
  },
  StackNavigationConfig
);

export default createAppContainer(RootNavigation);
