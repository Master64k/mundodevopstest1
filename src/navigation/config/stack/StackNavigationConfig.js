import React from 'react';
import {
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Colors from '../../../assets/palettes/default-palette';
import styles, { ICON_SIZE } from './styles';

const StackNavigationConfig = {
  initialRouteName: 'Profile',
  defaultNavigationOptions: {
    headerTitleStyle: styles.headerTitle,
    titleStyle: {
      alignSelf: 'center',
      textAlign: 'center'
    },
    headerStyle: styles.header,
    headerLeft: (
      <TouchableOpacity>
        <Icon
          name={'menu'}
          size={ICON_SIZE}
          color={Colors.text}
          style={styles.leftIcon}
        />
      </TouchableOpacity>
    ),
    headerRight: (
      <TouchableOpacity>
        <Icon
          name={'brightness-high'}
          size={ICON_SIZE}
          color={Colors.text}
          style={styles.rightIcon}
        />
      </TouchableOpacity>
    )
  }
};

export default StackNavigationConfig;
