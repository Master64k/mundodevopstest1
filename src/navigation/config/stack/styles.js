import { Platform, StyleSheet } from 'react-native';
import Colors from '../../../assets/palettes/default-palette';

export const ICON_SIZE = 24;

const styles = StyleSheet.create({
  headerTitle: {
    flexGrow: 1,
    fontFamily: 'Montserrat-Light',
    fontWeight: '100',
    textTransform: 'uppercase',
    color: Colors.text,
    textAlign: 'center',
    alignSelf: 'center'
  },
  header: {
    backgroundColor: Colors.background,
    ...Platform.select({
      ios: {
        shadowOpacity: 0
      },
      android: {
        elevation: 0
      }
    })
  },
  leftIcon: {
    marginLeft: 20
  },
  rightIcon: {
    marginRight: 20
  }

});

export default styles;
