
const IMAGES = {
  cycling: require('./cycling.png'),
  gym: require('./gym.png'),
  running: require('./running.png'),
  yoga: require('./yoga.png'),
  ic_dance: require('./ic_dance.png'),
  ic_lower_body: require('./ic_lower_body.png'),
  ic_upper_body: require('./ic_upper_body.png'),
  ic_yoga: require('./ic_yoga.png')
};

function getImage(imageName: string) {
  return IMAGES[imageName.split('.')[0]];
}

export default getImage;
