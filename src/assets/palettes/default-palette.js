/**
 *
 * Default palette
 *
 */

const Colors = {
  text: '#feffff',
  card: '#353D47',
  background: '#282F36',
  todayButton: '#fd3C29',
  yesterdayButton: '#20B996',
  horizontalListGradient: ['#7F26EF', '#CB2B7D'],
  horizontalListBackground: '#3f474e',
  dateInfoText: '#a0a4a9',
  dateInfoBorder: '#4d555e',
  dateInfoBackground: '#353e47',
  InfoContainerBorder: '#414850'

};

export default Colors;
