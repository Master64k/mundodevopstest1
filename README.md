# Mundodevops-test1

Essa é a implementação da tela de lista de exercícios baseada no protótipo anexado.

# Solução

* Foi criado um componente composto por duas FlatList's que encapsula o mecanismo de filtragem com base no nome de um atributo passado através de uma prop.
* O arquivo json foi reorganizado, de forma a criar uma estrutura hierárquica entre os filtros e os exercícios.

# Pre-requisitos
* Git
* Android SDK
* Java
* Node.js
* Yarn (`npm i -g yarn`)
* react-native-cli (`yarn global add react-native-cli`)


# Instalação:

    git clone https://Master64k@bitbucket.org/Master64k/mundodevopstest1.git
    cd  mundodevopstest1
    
# Como rodar:

### Android:

Instalar todas as dependências do projeto:
    
    yarn install

Com um emulador Android aberto ou dispositivo Android conectado,
executar o seguinte comando na pasta raiz do projeto:

    react-native run-android

# Autor 

Nícolas Costa (Master64k)